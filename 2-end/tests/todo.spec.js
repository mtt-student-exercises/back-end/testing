const { todoData, incompleteItems } = require("./todo.data");

const { getIncompleteItems, getUserIds, getAllUsers } = require("../todo/todo");

test("getIncompleteItems() returns only incomplete TODO items", () => {
  const incompleteTestItems = getIncompleteItems(todoData);

  expect(incompleteTestItems).toStrictEqual(incompleteItems);
});

describe("getUserIds()", () => {
  test("Returns an array", () => {
    const userIds = getUserIds(todoData);

    expect(Array.isArray(userIds)).toBe(true);
  });

  test("Returns users in data set", () => {
    const userIds = getUserIds(todoData);

    expect(userIds).toStrictEqual([1, 2, 3]);
  });
});

describe("getAllUsers()", () => {
  test("Returns list of users from API", () => {
    getAllUsers().then((users) => {
      expect(users).toStrictEqual(todoData);
    });
  });
});
