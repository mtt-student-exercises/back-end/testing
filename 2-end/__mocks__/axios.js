const { todoData } = require("../tests/todo.data");

module.exports = {
  get: jest.fn(() => {
    return Promise.resolve({
      data: todoData,
    });
  }),
};
