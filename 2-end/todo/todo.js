const axios = require("axios");

function getIncompleteItems(todoData) {
  return todoData.filter((item) => item.completed === false);
}

function getUserIds(todoData) {
  const users = todoData.map((todo) => todo.userId);

  return [...new Set(users)];
}

function getAllUsers() {
  return new Promise((resolve, reject) => {
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => reject(err));
  });
}

module.exports = {
  getIncompleteItems,
  getUserIds,
  getAllUsers,
};
