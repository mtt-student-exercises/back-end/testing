const { getAllUsers } = require("./todo/todo");

const startTime = new Date();

getAllUsers()
  .then((data) => {
    console.log(data);

    console.log(`Executed in: ${new Date() - startTime}ms`);
  })
  .catch((err) => console.log(err));
